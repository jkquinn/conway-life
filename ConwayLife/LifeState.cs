﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConwayLife
{
    public class LifeState
    {
        public LifeState(int LifeRows, int LifeColumns)
        {
            Rows = LifeRows;
            Columns = LifeColumns;
            Values = new bool[Rows,Columns];
            Randomize();
        }

        public LifeState(String xmlFile)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            Columns = doc.SelectNodes("Grid/Col").Count;
            Rows = doc.SelectNodes("Grid/Col/Row").Count / Columns;

            int columnCount = 0;
            int rowCount = 0;

            bool[,] loadedValues = new bool[Rows, Columns];

            foreach (XmlNode colNode in doc.DocumentElement.ChildNodes)
            {
                foreach (XmlNode rowNode in colNode.ChildNodes)
                {
                    String innerTextVal = rowNode.InnerText.Trim();
                    if (innerTextVal == "true")
                        loadedValues[rowCount, columnCount] = true;
                    else
                        loadedValues[rowCount, columnCount] = false;

                    rowCount++;
                }
                rowCount = 0;
                columnCount++;
            }

            Values = loadedValues;
        }

        public int Rows
        {
            get;
            private set;
        }
        public int Columns
        {
            get;
            private set;
        }
        public bool[,] Values
        {
            get;
            private set;
        }

        public void Randomize()
        {
            Random Randomizer = new Random(Environment.TickCount);

            for(int RowCounter = 0; RowCounter < Rows; RowCounter++)
            {
                for(int ColumnCounter = 0; ColumnCounter < Columns; ColumnCounter++)
                {
                    switch(Randomizer.Next(2))
                    {
                        case 1:
                            Values[RowCounter,ColumnCounter] = true;
                            break;
                        default:
                            Values[RowCounter, ColumnCounter] = false;
                            break;
                    }
                }
            }
        }

        public void Advance()
        {
            bool[,] nextStateValues = new bool[Rows, Columns];
            const int MIN_POPULATION_VAL= 2;
            const int MAX_POPULATION_VAL = 3;

            for (int RowCounter = 0; RowCounter < Rows; RowCounter++)
            {
                for (int ColumnCounter = 0; ColumnCounter < Columns; ColumnCounter++)
                {
                    int sum = sumOfNeighbors(RowCounter, ColumnCounter);

                    if (true == Values[RowCounter, ColumnCounter])
                    { 
                        if (sum < MIN_POPULATION_VAL || sum > MAX_POPULATION_VAL)
                        {
                            //cell dies
                            nextStateValues[RowCounter, ColumnCounter] = false;
                        }
                        else
                        {
                            nextStateValues[RowCounter, ColumnCounter] = true;
                        }
                    }
                    else
                    {
                        if (sum == MAX_POPULATION_VAL)
                        {
                            //cell birth
                            nextStateValues[RowCounter, ColumnCounter] = true;
                        }
                        else
                        {
                            nextStateValues[RowCounter, ColumnCounter] = false;
                        }
                    }
                }
            }

            //update the live cells values
            Values = nextStateValues;
        }

        //Check surrounding neighbor's values and increment sum value
        private int sumOfNeighbors(int row, int column)
        {
            int sum = 0;

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int neighborRow = row + i;
                    int neighborCol = column + j;

                    //check bounds
                    if ((neighborRow >= 0 && neighborRow < Rows) && 
                        (neighborCol >= 0 && neighborCol < Columns))
                    {
                        //if not referring to itself, increment the sum only if live cell
                        if (!((row + i == row) && (column + j == column)))
                        {
                            if (Values[row + i, column + j])
                                sum++;
                        }
                    }
                }
            }

            return sum;
        }
    }
}
