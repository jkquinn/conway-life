﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace ConwayLife
{
    [TestFixture]
    class LifeStateTest
    {
        LifeState GliderState;
        LifeState BlockState;

        readonly String dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        [SetUp]
        public void initRunState()
        {
            GliderState = new LifeState(dir + "/glider.xml");
            BlockState = new LifeState(dir + "/block.xml");
        }

        [Test]
        [Category("pass")]
        public void advanceBlock()
        {
            Assert.AreEqual(true, BlockState.Values[2, 0]);
            GliderState.Advance();
            Assert.AreEqual(true, BlockState.Values[2, 0]);
            GliderState.Advance();
            Assert.AreNotEqual(false, BlockState.Values[2, 0]);
        }

        [Test]
        [Category("pass")]
        public void advanceGlider()
        {
            Assert.AreEqual(true, GliderState.Values[2, 1]);
            GliderState.Advance();
            Assert.AreEqual(true, GliderState.Values[2, 1]);
            GliderState.Advance();
            Assert.AreEqual(false, GliderState.Values[2, 1]);
        }
    }
}
